<?php

if(!function_exists('_var_dump')){
	function _var_dump($var) {
		static $index = 0;
		$index++;
		$arguments = func_get_args();

		if($index == 1){
	?>
			<style>
				pre._var_dump{
					position: relative;
					display: block;
					max-width: 100%;
					font-size: 14px;
					font-weight: normal;
					text-transform: none;
					color: black;
					background: white;
					border-radius: 5px;
					padding: 15px;
					margin: 20px 10px;
					line-height: 1.5em;
					word-spacing: 0.8em;
					overflow: auto;
					z-index: 999;
				}
				pre._var_dump ._vd_block + ._vd_block{
					margin-top: 2em;
				}
				pre._var_dump .open-bkt:focus {
					outline: 1px solid #eaeaea;
				}
				pre._var_dump .open-bkt {
					display: inline;
				}
				pre._var_dump .bkt-lvl {
					font-size: 0.8em;
				}
				pre._var_dump .open-bkt:focus > .bkt, pre._var_dump .open-bkt:focus > .bkt-lvl {
					color: red;
					font-weight: bold;
				}
			</style>
	<?php
		}

			echo '<pre class="_var_dump">';
				foreach ($arguments as $value) {
					echo '<div class="_vd_block">';
					$debug_backtrace = debug_backtrace();
					echo $debug_backtrace[0]['file'] . ':' . $debug_backtrace[0]['line'] . "\n";
						ob_start();
						var_dump($value);
						$var_dump = ob_get_clean();
						$var_dump = preg_replace('/=>\s+/i', '=>', $var_dump);
						$var_dump = preg_replace_callback('/\s{0,}\{\n{0,}|\s{0,}\}\n{0,}/', function($matches){
							if(strpos($matches[0], '{') !== false){
								$v = str_replace('{', '<div tabindex="0" class="open-bkt"><span class="bkt">{</span>', $matches[0]);
							}else{
								$v = str_replace('}', '<span class="bkt">}</span></div>', $matches[0]);
							}
							return $v;
						}, $var_dump);
						echo $var_dump;
					echo '</div>';
				}
			echo '</pre>';
			?>
	<?php }
}

/* return or echo */
function r_e($result, $echo){

	if($echo){
		echo $result;
	}else{
		return $result;
	}

}

if(!function_exists('isInt')){
	function isInt($value){
		if(!is_numeric($value))
			return false;
		if(!is_string($value) && !is_int($value))
			return false;
		if((string) $value !== '0' && empty($value))
			return false;

		if($value < 0)
			$value = abs($value);

		return ctype_digit(strval( $value ));
	}
}

if(!function_exists('theme_current_url')){
	function theme_current_url() {
		$url  = @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["SERVER_NAME"] :  'https://'.$_SERVER["SERVER_NAME"];
		if($_SERVER["SERVER_PORT"] != 80 && $_SERVER["SERVER_PORT"] != 443){
			$url .= ":".$_SERVER["SERVER_PORT"];
		}
		$url .= $_SERVER["REQUEST_URI"];
		return $url;
	}
}

/*
$args = array(
	'name'=>'new_value', //Обновляем [string] or [array]
	'name'=>false, //Удаляем
	'old_value', //Удаляем по значению
	'edit'=>null, //Оставляем только параметр
);
*/
if(!function_exists('change_query')){
	function change_query($args = array(), $url = ''){
		if(!$url){
			$url = theme_current_url();
		}
		$url = stripslashes(htmlspecialchars_decode($url));
		$parse_url = parse_url($url);
		$query = array();
		if(isset($parse_url['query']) && $parse_url['query'])
			parse_str($parse_url['query'], $query);

		if($args)
			foreach($args as $key => $value){

				switch(true){
					case $value === false: //Удилить
						unset($query[$key]);
					break;

					case is_numeric($key): //Удалить по значению
						$value = (array) $value; //Если строка, то преобразуем в массив
						$query = array_diff($query, $value);
					break;

					case $value === null: //Оставляем пустым
						$query[$key] = '';
					break;

					case true: //Обновляем
						if(!empty($value)){
							$query[$key] = $value;
						}else{
							unset($query[$key]);
						}
					break;
				}

			}

		$http = $parse_url['scheme']? $parse_url['scheme'].'://' : '';

		if(!$query) //Нечего подставить в запрос
			return $http.$parse_url['host'].$parse_url['path'];

		return $http.$parse_url['host'].$parse_url['path'].'?'.preg_replace('/=(&|$)/', '$1', http_build_query($query));
	}
}

/**
 * [theme_attr Функция содает html атрибты для тегов]
 * @param  array|string $array
 * @param  string|bool|null $value [Значение атрибута указанного в первом аргументе функции, если это массив то он будет преобразован в json объект и закодирован с помощью urlencode. Булевое значение указывается только если первый аргумент является массивом, используется вместо аргумена $echo]
 * @param  bool $echo [Echo - true, return - false. Default: false]
 * @return string        [html атрибуты]
 * 
 * ps. чтобы раскодировать закодированный массив: JSON.parse(decodeURIComponent(value))
 * 
 * theme_attr(array('attr1', 'attr2' => null, 'attr3' => false, 'attr4' => true , 'attr5' => "123", 'attr6' => "" )) => attr1="attr1" attr2="attr2" attr4="" attr5="123"
 * theme_attr('attr1') => attr1="attr1"
 * theme_attr('attr2', false) => ''
 * theme_attr('attr3', '') => ''
 * theme_attr('attr4', array(1 => 2)) => attr4="%7B%221%22%3A2%7D" => JSON.parse(decodeURIComponent('%7B%221%22%3A2%7D')) => {1: 2}
 */
if(!function_exists('theme_attr')){
	function theme_attr($array, $value = null, $echo = 0) {
		if(!$array) return false;

		$attrs = array();
		if(is_array($array)){
			if($array)
				foreach ($array as $key => $array_value) {
					if($array_value || is_numeric($array_value) || $array_value === null)
						if(is_int($key)){
							if($array_value)
								$attrs[] = "{$array_value}=\"{$array_value}\"";
						}else{
							if(is_array($array_value)){ //Если это массив для кодирования
								$array_value = urlencode(json_encode($array_value));
								$attrs[] = "{$key}=\"{$array_value}\"";
							}else{
								if($array_value === null){
									$attrs[] = "{$key}=\"{$key}\"";
								}else{
									$attrs[] = "{$key}=\"{$array_value}\"";
								}
							}
						}
				}
		}else{
			if($value === null){
				$attrs[] = "{$array}=\"{$array}\"";
			}else{
				if($value || is_numeric($value)){
					if(is_array($value)) //Если это массив для кодирования
						$value = urlencode(json_encode($value));
					$attrs[] = "{$array}=\"{$value}\"";
				}
			}
		}

		$result = implode(' ', $attrs);

		if(is_array($array))
			$echo = (bool) $value;

		if($echo){
			echo $result;
		}else{
			return $result;
		}
	}
}

/**
 * word_declension Склонение строки по числу
 * @param  integer $num     Число по которому будет производиться склонение
 * @param  string|array  $str_0   Первое значение склоненного числа (ноль бегемотов). Или массив вида array(0 => $str_0, 1 => $str_1, 2 => $str_2)
 * @param  string|bool  $str_1   Второе значение склоненного числа (один бегемот). Если $str_0 массив то используется как $sprintf
 * @param  string  $str_2   Третье значение склоненного числа (два бегемота).
 * @param  bool $sprintf Использовать ли функцию sprintf для вывода "%d бегимотов"
 * @return Склоненная строка
 */
if(!function_exists('word_declension')){
	function word_declension($num = 0, $str_0 = null, $str_1 = null, $str_2 = null, $sprintf = false) {
	/*
		$str_0 - (ноль бегемотов)
		$str_1 - (один бегемот)
		$str_2 - (два бегемота)
	*/

		if(is_array($str_0)){
			$sprintf = $str_1? true : false;
			$str_0 = $str_0[0];
			$str_1 = $str_0[1];
			$str_2 = $str_0[2];
		}

		if($num == null) 
			if($sprintf)
				return sprintf($str_0, 0);
			else
				return '0 '. $str_0;

		$val = abs($num) % 100;

		if ($val > 10 && $val < 20){
			if($sprintf)
				return sprintf($str_0, $num);
			else
				return $num .' '. $str_0;
		}else{
			$val = abs($num) % 10;
			if ($val == 1){
				if($sprintf)
					return sprintf($str_1, $num);
				else
					return $num .' '. $str_1;
			}
			elseif($val > 1 && $val < 5){
				if($sprintf)
					return sprintf($str_2, $num);
				else
					return $num .' '. $str_2;
			}else{
				if($sprintf)
					return sprintf($str_0, $num);
				else
					return $num .' '. $str_0;
			}
		}
	}
}