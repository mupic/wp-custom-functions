<?php

function theme_image_url($path = '', $echo = false){
	$result = get_stylesheet_directory_uri().'/images/'.$path;

	return r_e($result, $echo);
}

function theme_image($path = '', $attrs = '', $echo = true){
	$url = theme_image_url($path, false);

	$result = '<img src="'.$url.'" '.theme_attr($attrs).'>';

	return r_e($result, $echo);
}

function theme_image_path($path = '', $echo = false){
	$result = get_stylesheet_directory().'/images/'.$path;

	return r_e($result, $echo);
}

function theme_include_svg($file = '', $echo = true){
	if(!file_exists($file))
		return false;

	$result = file_get_contents($file);

	return r_e($result, $echo);
}

function theme_svg($name_file = '', $echo = true){
	$result = theme_include_svg(theme_image_path($name_file, false), false);

	return r_e($result, $echo);
}

function theme_get_current_user($current_user = null){
	switch(true){
		case ($current_user instanceof WP_User):
		break;
		case isInt($current_user) && $current_user: //если id
			$current_user = get_userdata($current_user);
		break;
			
		case is_object($current_user) && isset($current_user->comment_ID):  //Если комментарий
			$current_user = get_userdata($current_user->user_id);
		break;
			
		case !is_object($current_user) && is_email($current_user): //Если почта
			$current_user = get_user_by('email', $current_user);
		break;
			
		case ( $current_user instanceof WP_Post ):
			$current_user = get_user_by( 'id', (int) $current_user->post_author );
		break;
			
		case !is_object($current_user): //Если ничего
			$current_user = wp_get_current_user();
		break;
	}
	
	return $current_user;
}

function theme_post_id($post_id = null){

	switch(true){
		case isInt($post_id) && $post_id: //если id
		break;
			
		case ( $post_id instanceof WP_Post ):
			$post_id = $post_id->ID;
		break;
			
		case !$post_id: //Если ничего
			global $post;
			$post_id = $post->ID;
		break;
	}

	return $post_id;
}

function theme_the_excerpt( $charlength, $content = null, $echo = 1 ){
	if(!$content)
		if(function_exists('lang_filter_content')){
			$content = lang_filter_content(get_the_content(''));
		}else{
			$content = get_the_content('');
		}

	$excerpt = strip_tags($content);
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			$result = mb_substr( $subex, 0, $excut ) . '...';
		} else {
			$result = $subex . '...';
		}
	}else{
		$result = $excerpt;
	}

	return r_e($result, $echo);
}

/**
 * theme_get_style_bg_thumbnail Выводит css свойство для вставки в атрубит style
 * @param  [null|int|WP_Post] $id [int - id вложения. WP_Post - объект записи. null - получит объект записи если функция внутри цикла]
 * @param  [string]  $size       [размер изображения]
 * @param  [string]  &$image_src [Присваивает ссылку на файл]
 * @return [string]              [description]
 */
function theme_get_style_bg_thumbnail($id = false, $size = 'thumbnail', &$image_src = ''){
	if(is_object($id)){
		$post = $id;
	}else{
		global $post;
	}
	if(!$id){
		$id = get_post_thumbnail_id( $post );
	}

	//is_numeric - если вставили id файла
	//!empty - если пытаемся получить вложение записи
	if(!is_numeric($id) && !empty($post) && function_exists('theme_attachment_translated_src')){
		$image_attributes = theme_attachment_translated_src($post->ID, $size);
	}else{
		$image_attributes = wp_get_attachment_image_src($id, $size);
	}

	$image_src = $image_attributes[0];
	$thumbnail = $image_attributes? "background-image: url({$image_src});" : '';

	return $thumbnail;
}


/**
 * theme_relative_url Получает url до указанного файла относительно серверного файла указанного в 1 аргументе. Должна вызываться внутри темы.
 * @param  string $__file__  Путь до файла где вызывается функция __FILE__
 * @param  string $path      Относительный путь до нужного файла.
 * @return string            URL до указанного файла.
 */
function theme_relative_url($__file__, $path = ''){
	$__file__ = trailingslashit(dirname($__file__));
	$path = $path;
	$theme_dir = wp_normalize_path(trailingslashit(get_stylesheet_directory()));
	$theme_url = trailingslashit(get_stylesheet_directory_uri());

	$full_path = wp_normalize_path(realpath($__file__ . $path));

	$folder = str_replace($theme_dir, '', $full_path);
	return $theme_url . $folder;
}